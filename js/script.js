$(document).ready(function(e) {
		$wi = $(document).width();
		if($wi < 1200 && $wi > 600)
		{
			$(".menu").css("margin-top","8%");	
			$(".menu img").css("width","92.5%");	
			$("#item2,#item3,#item4").css("margin-top","3%");	
			$("#item5").css("margin-top","2%");	
		}
		if($wi < 600)
		{
			$(".menu").css("margin-top",	"8%");	
			$(".menu").css("margin-left","2.5%");	
			$(".menu img").css("width","88%");	
			$("#item2,#item3,#item4").css("margin-top","2.5%");	
			$("#item5").css("margin-top","2%");	
		}
		
		$devWidth = $(document).width()-40;
		$devHeight = $(document).height()-40;
		
		$("#item1").mousedown(function(e) {
		clearTimeout(this.downTimer);
		this.downTimer = setTimeout(function() {			
			$("#item1 img").css("transform","initial");
			$("#about").css({'z-index':'1000','transform':'scale(0.8,0.8) translateY(300px) rotateX(-360deg)'});
			
			resize();
		}, 1000);
				
		
		}).mouseup(function(e) {
			clearTimeout(this.downTimer);
		});		
		
		$("#about").click(function(e) {
			$("#item1 img").css("transform","rotateX(89deg)");
			$("#about").css({'transform':'initial','z-index':'-1'});
			setTimeout(function(){
			$("#item1 img").css("transform","rotateX(0deg)");
			},2000);
        });
		
		function resize()
		{
			setTimeout(function(){
				$("#about").css({'transform':'scale(2,6) translateY(40px)'});
			},2500);
		}
    });
